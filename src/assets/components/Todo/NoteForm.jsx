/* eslint-disable react/prop-types */


const NoteForm = ({ getAllNotes, ...props }) => {

  const {
    todoTitle,
    setTodoTitle,
    todoLists,
    setTodoLists,
    editMode,
    editableNote,
    setEditMode,
    setEditableNote
  } = props;


  // Submit Handler
  const submitHandler = (event) => {
    event.preventDefault();
    if (todoTitle.trim() === "") return alert("Add Todo Item");

    editMode ? updateHandler() : createHandler();
  }

  // Create Handler
  const createHandler = () => {
    const newTodo = {
      id: Date.now() + "",
      title: todoTitle,
      status: "Pending",
      clicked: true
    }

    fetch(`http://localhost:5000/notes`, {
      method: "POST",
      body: JSON.stringify(newTodo),
      headers: {
        "Content-type": "application/json"
      }
    }).then(() => {
      getAllNotes()
    })
    .catch((err) => console.error('Error creating note:', err));

    // setTodoLists([...todoLists, newTodo]);
    setTodoTitle("");
    console.log("created");
  }

  // Update Handler
  const updateHandler = () => {
    const updateTodoList = {...editableNote, title: todoTitle}

    fetch(`http://localhost:5000/notes/${updateTodoList.id}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(updateTodoList),
    }).then(() => {
      getAllNotes();
    })
    
    setEditMode(false);
    setEditableNote(null)
    setTodoTitle("");
  }

  return (

    <div>
      <form onSubmit={submitHandler} className="form">
        <input type="text" value={todoTitle} onChange={(event) => setTodoTitle(event.target.value)} />
        <button>{editMode ? "Update Item" : "Add Item"}</button>
      </form>
    </div>


  )
}

export default NoteForm