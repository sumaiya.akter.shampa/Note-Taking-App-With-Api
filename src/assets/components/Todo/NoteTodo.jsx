import { useEffect, useState } from "react";
import './todo.css';
import NoteForm from "./NoteForm";
import NoteList from "./NoteList";

const NoteTodo = () => {
  const [todoTitle, setTodoTitle] = useState("");
  const [todoLists, setTodoLists] = useState([
    { id: "1156", title: "Task One", status: "completed" }
  ]);
  const [editMode, setEditMode] = useState(false);
  const [editableNote, setEditableNote] = useState(null);

  
  const getAllNotes = () => {
    fetch(`http://localhost:5000/notes`)
    .then((res) => res.json())
    .then((data) => {
      setTodoLists(data)
    })
    .then((data) => console.log(data))
  }


  useEffect(() => {
    getAllNotes();
  },[])


  return (
    <div className="todo">
      <h1 className="text-white">Note Taking App</h1>
      <NoteForm
        todoTitle={todoTitle} 
        setTodoTitle={setTodoTitle} 
        todoLists={todoLists} 
        setTodoLists={setTodoLists} 
        editMode={editMode}
        editableNote={editableNote}
        setEditMode={setEditMode}
        setEditableNote={setEditableNote}
        onButtonClick={getAllNotes}
        getAllNotes={getAllNotes}
      />

      <NoteList
        setTodoTitle={setTodoTitle}
        todoLists={todoLists}
        setTodoLists={setTodoLists}
        setEditMode={setEditMode}
        setEditableNote={setEditableNote}
        getAllNotes={getAllNotes}
      />
    </div>
  )
}

export default NoteTodo;